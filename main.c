// test program for ads1256

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ads1256.h"
#include "dac8532.h"
#include "board.h"


#define PGA (1)

void handler(int signo)
{
    printf("Caught SIGINT signal, exiting...\n");
//    ads1256_enter_standby_mode();
//    dac8532_power_down();
    gpio_close();
    exit(0);
}

// scan 8 bits with leading zeroes
// display in two 4 bit segments
void binsprintf(char *s,int val)
{
    char *p;
    p = s;
    for(unsigned int t = 0x80; t > 0; t = t >> 1) {
        if (t == 0x8)
	    *p++ = ' ';

	if(val & t)
	    *p++ = '1';
	else
	    *p++ = '0';
    }
    *p=0;
}

char str[24];

int main(int argc, char** argv)
{
    FILE *file_ptr;
    uint8_t channel = 0;
//    uint32_t value = 0;
    
    signal(SIGINT, handler);
    
    gpio_init();
    
    if (ads1256_init(PGA_GAIN_1, DRATE_7500SPS) != 0) {
        printf("main: ads1256 Initialization failed!\n");
        gpio_close();
	exit(1);
    }

    uint8_t status = ads1256_read_status_register();
    binsprintf(str, status);
    printf("Status Register: %s (0x%02x)\n", str, status);

    uint8_t mux = ads1256_read_register(REG_MUX);
    binsprintf(str, mux);
    printf("mux register: %s (0x%02x)\n", str, mux);

    uint8_t adcon = ads1256_read_register(REG_ADCON);
    binsprintf(str, adcon);
    printf("ADCON Register: %s (0x%02x)\n", str, adcon);

    uint8_t drate = ads1256_read_register(REG_DRATE);
    binsprintf(str, drate);
    printf("DRATE Register: %s (0x%02x)\n\n", str, drate);

//    while (1) {
//        value = ads1256_get_sample_from_channel(channel);
//        printf("ADC 0: %.2f\r", convert_to_volts(value));//((2 * VREF) / OFFSET) * value / (pow(2, PGA)));
//
//        printf("\33[1B");
//        float x = (value >> 7) * 5.0 / 0xffff;
//        printf("DAC A: %.2f\r", x);
//        printf("\33[1A");
//
//        dac8532_voltage_out(DAC_LDAB_BUFFER_B, x);
//        dac8532_voltage_out(DAC_LDAB_BUFFER_A, (3.3 - x));
//    }

    uint32_t n_samples = 8192;
    uint32_t* samples = (uint32_t*)malloc(n_samples * sizeof(uint32_t));
    memset(samples, 0, n_samples * sizeof(uint32_t));
    
    ads1256_get_n_samples(channel, samples, n_samples);

    if ((file_ptr = fopen("test_data.bin", "w")) == NULL) {
        printf("main: Failed to open file for writing\n");
        return 1;
    }
    fwrite(samples, sizeof(uint32_t), n_samples, file_ptr);
    fclose(file_ptr);

    free(samples);

    return 0;
}
