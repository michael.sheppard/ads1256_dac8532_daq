#ifndef _DAC8532_h_
#define _DAC8532_h_

#include "board.h"

#define DAC_LDAB_BUFFER_A 0x30
#define DAC_LDAB_BUFFER_B 0x34

#define DAC_LDA 0x10
#define DAC_LDB 0x20

#define DAC_BUFFER_A 0x00
#define DAC_BUFFER_B 0x04

#define DAC_VALUE_MAX 65535
#define DAC_VREF 3.3

void dac8532_write_value(uint8_t channel, uint16_t value);
void dac8532_voltage_out(uint8_t channel, float voltage);
void dac8532_power_down();
void dac8532_power_up();

#endif // _DAC8532_h_
