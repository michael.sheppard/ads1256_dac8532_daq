CC=gcc
CFLAGS=-Wall -I.

LIBS=-lm -lbcm2835

DEPS = ads1256.h dac8532.h

OBJ = ads1256.o dac8532.o main.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) $(LIBS)

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f *.o main
