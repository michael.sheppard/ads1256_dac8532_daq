#include <stdio.h>
#include <stdint.h>
#include <math.h>


void main()
{
    uint32_t val = 1;
    uint8_t buffer[3] = {0, 0, 0};

    int offset = pow(2, 23);
    printf("val * 5.0 / 0x7fffff: %0.16f\n", val * 5.0 / 0x7fffff);
    printf("val * 10.0 / 0x7fffff: %0.16f\n", val * 10.0 / 0x7fffff);
    printf("(8388608 - offset) * 10.0 / 0x7fffff: %0.16f\n", (8388608-offset) * 10.0 / 0xf77777);
    printf("sizeof(buffer): %d\n", sizeof(buffer));
    uint16_t divider = (uint16_t)((uint32_t)250000000 / 1920000);
    divider &= 0xfffe;
    printf("SPI clk Hz(divider): %d\n", divider);
}
