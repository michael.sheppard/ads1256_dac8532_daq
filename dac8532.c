#include "dac8532.h"

void dac8532_write_value(uint8_t channel, uint16_t value) 
{
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 0);

    bcm2835_spi_transfer(channel);
    bcm2835_spi_transfer(value >> 8);
    bcm2835_spi_transfer(value & 0xff);

    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
}

void dac8532_voltage_out(uint8_t channel, float voltage)
{
    uint16_t tmp = 0;

    if ((voltage <= DAC_VREF) && voltage >= 0) {
        tmp = (uint16_t)(voltage * DAC_VALUE_MAX / DAC_VREF);
        dac8532_write_value(channel, tmp);
    }
}

// power down DACA and DACB to high impedance sequentially
void dac8532_power_down()
{
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 0);

    uint8_t command[4];
    command[0] = 0x13;
    command[1] = 0x00;
    command[2] = 0x27;
    command[3] = 0x00;

    bcm2835_spi_transfer(command[0]);
    bcm2835_spi_transfer(command[1]);
    bcm2835_spi_transfer(command[2]);
    bcm2835_spi_transfer(command[3]);

    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
}

void dac8532_power_up()
{
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
    bcm2835_gpio_write(DEVICE_SYNC_PIN, 0);

    uint8_t command[4] = {0, 0, 0, 0};

    bcm2835_spi_transfer(command[0]);
    bcm2835_spi_transfer(command[1]);
    bcm2835_spi_transfer(command[2]);
    bcm2835_spi_transfer(command[3]);

    bcm2835_gpio_write(DEVICE_SYNC_PIN, 1);
}
