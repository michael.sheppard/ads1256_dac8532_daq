// Michael Sheppard 04072023
// implements Texas Instruments ADS1256 on a Waveshare
// High Precision AD-DA HAT for Raspberry PI
#ifndef _ADS1256_H_
#define _ADS1256_H_

#include <bcm2835.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#define DEVICE_RST_PIN 18 // ads1256 reset pin
#define DEVICE_CS0_PIN 22 // ads1256 chip select
#define DEVICE_SYNC_PIN 23 // dac8532 sync pin (dac chip select)
#define DEVICE_DRDY_PIN 17 // ads1256 data ready pin

#define DRDY_TIMEOUT 4000000
#define SCANMODE_SINGLE_ENDED 0
#define SCANMODE_DIFFERENTIAL 1

#define VREF 2.50
#define OFFSET (8388608)

//#define SHIFT(h, m, l) ((((uint32_t) h << 16) & 0x00ff0000) | ((uint32_t) m << 8) | (l))

#define shift(h, m, l) ((((uint32_t) h << 16) & 0x00ff0000) | ((uint32_t) m << 8) | (l))


#define ADC2V 0.0000005960465188 // (input_voltage_range / full scale ADC output)

enum ADS1256_CMDS {
    CMD_WAKEUP  = 0x00,	// Completes SYNC and Exits Standby Mode 0000  0000 (00h)
    CMD_RDATA   = 0x01, // Read Data 0000  0001 (01h)
    CMD_RDATAC  = 0x03, // Read Data Continuously 0000   0011 (03h)
    CMD_SDATAC  = 0x0F, // Stop Read Data Continuously 0000   1111 (0Fh)
    CMD_RREG    = 0x10, // Read from REG rrr 0001 rrrr (1xh)
    CMD_WREG    = 0x50, // Write to REG rrr 0101 rrrr (5xh)
    CMD_SELFCAL = 0xF0, // Offset and Gain Self-Calibration 1111    0000 (F0h)
    CMD_SELFOCAL= 0xF1, // Offset Self-Calibration 1111    0001 (F1h)
    CMD_SELFGCAL= 0xF2, // Gain Self-Calibration 1111    0010 (F2h)
    CMD_SYSOCAL = 0xF3, // System Offset Calibration 1111   0011 (F3h)
    CMD_SYSGCAL = 0xF4, // System Gain Calibration 1111    0100 (F4h)
    CMD_SYNC    = 0xFC, // Synchronize the A/D Conversion 1111   1100 (FCh)
    CMD_STANDBY = 0xFD, // Begin Standby Mode 1111   1101 (FDh)
    CMD_RESET   = 0xFE, // Reset to Power-Up Values 1111   1110 (FEh)
};

enum ADS1256_GAINS {
    PGA_GAIN_1      = 0,
    PGA_GAIN_2	= 1,
    PGA_GAIN_4	= 2,
    PGA_GAIN_8	= 3,
    PGA_GAIN_16	= 4,
    PGA_GAIN_32	= 5,
    PGA_GAIN_64	= 6,
};

enum ADS1256_REGISTERS {
    REG_STATUS = 0x00,	// x1H reset values
    REG_MUX    = 0x01,  // 01H
    REG_ADCON  = 0x02,  // 20H
    REG_DRATE  = 0x03,  // F0H
    REG_IO     = 0x04,  // E0H
    REG_OFC0   = 0x05,  // xxH
    REG_OFC1   = 0x06,  // xxH
    REG_OFC2   = 0x07,  // xxH
    REG_FSC0   = 0x08,  // xxH
    REG_FSC1   = 0x09,  // xxH
    REG_FSC2   = 0x0a,  // xxH
};

#define DRATE_30000SPS    0xf0
#define DRATE_15000SPS    0xe0
#define DRATE_7500SPS     0xd0
#define DRATE_3750SPS     0xc0
#define DRATE_2000SPS     0xb0
#define DRATE_1000SPS     0xa1
#define DRATE_500SPS      0x92
#define DRATE_100SPS      0x82
#define DRATE_60SPS       0x72
#define DRATE_50SPS       0x63
#define DRATE_30SPS       0x53
#define DRATE_25SP        0x43
#define DRATE_15SPS       0x33
#define DRATE_10SPS       0x20
#define DRATE_5SPS        0x13
#define DRATE_2d5SPS      0x03

uint8_t ads1256_init(uint8_t gain, uint8_t Rs);
uint8_t ads1256_read_chip_id();
void ads1256_wait_drdy_timeout();
void ads1256_set_mode(uint8_t mode);
void ads1256_write_command(uint8_t cmd);
void ads1256_configure_adc(uint8_t gain, uint8_t Rs);
void ads1256_set_channel(uint8_t channel);
void ads1256_reset();
void ads1256_reset_cmd();
void ads1256_write_register(uint8_t reg, uint8_t value);
uint8_t ads1256_read_register(uint8_t reg);
void ads1256_get_n_samples(uint8_t channel,  uint32_t* samples, uint32_t nSamples);
uint32_t ads1256_get_single_sample(void);
uint32_t ads1256_get_sample_from_channel(uint8_t channel);
void ads1256_enter_standby_mode();
uint8_t ads1256_read_status_register();
void ads1256_set_channel_differential(uint8_t channel);
void ads1256_get_all_channels(uint32_t *values);
uint32_t ads1256_get_single_continuous();
void ads1256_stop_continuous();
float convert_to_volts(uint32_t value);

uint8_t gpio_init();
void gpio_close();
void spi_init();
void spi_close();

#endif // _ADS1256_H_
