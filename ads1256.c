// Michael Sheppard 04072023
// implements Texas Instruments ADS1256 on a Waveshare
// High Precision AD-DA HAT for Raspberry PI

#include "ads1256.h"

uint8_t scan_mode = SCANMODE_SINGLE_ENDED;
bool continuous_mode = false;


uint8_t gpio_init()
{
    if (!bcm2835_init()) {
        printf("bcm2835_init() failed!\n");
        return 1;
    } 

    // configure output pins
    bcm2835_gpio_fsel(DEVICE_RST_PIN, BCM2835_GPIO_FSEL_OUTP); // ads1256
    bcm2835_gpio_fsel(DEVICE_CS0_PIN, BCM2835_GPIO_FSEL_OUTP); // ads1256 chip select
    bcm2835_gpio_fsel(DEVICE_SYNC_PIN, BCM2835_GPIO_FSEL_OUTP); // dac8532 chip select

    // configure input pins
    bcm2835_gpio_fsel(DEVICE_DRDY_PIN, BCM2835_GPIO_FSEL_INPT); // ads1256
    
    // configure SPI
    spi_init();

    return 0;
}

void gpio_close()
{
    spi_close();
    bcm2835_close();
}

void spi_init()
{
    bcm2835_spi_begin();
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST); // set bit order MSB first
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE1); // set SPI mode
    // on raspi 3 system clk is 400MHz
    // 400Mhz/64 = 6.250MHz SPI
    // 400MHz/32 = 12.5MHz SPI
    // 400MHz/16 = 25MHz SPI
    // BCM2835_SPI_CLOCK_DIVIDER_8 is the fastest reliable speed at 50MHz on Raspi 3
//    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_256); // set the SPI bus speed
    bcm2835_spi_set_speed_hz(1920000); // set the SPI speed in Hz
    bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);
}

void spi_close()
{
    bcm2835_spi_end();
}


uint8_t ads1256_init(uint8_t gain, uint8_t drate)
{
    ads1256_reset();
    uint8_t chip_id = ads1256_read_chip_id();
    if (chip_id == 3) {
        printf("ADS1256 Chip ID: 0x%0x\n", chip_id);
    } else {
        printf("ADS12567 Chip ID Read failed\n");
        return 1;
    }
    ads1256_configure_adc(gain, drate);
    continuous_mode = false;
    
    return 0;
}

uint8_t ads1256_read_chip_id()
{
    ads1256_wait_drdy_timeout();
    uint8_t id = ads1256_read_register(REG_STATUS);
    return id >> 4;
}

void ads1256_wait_drdy_timeout()
{
    uint32_t counter = 0;
    do {
        if (bcm2835_gpio_lev(DEVICE_DRDY_PIN) == LOW) {
            break;
        }
        counter++;
    } while (counter < DRDY_TIMEOUT);
}

// mode can be 
// SCANMODE_SINGLE_ENDED
// SCANMODE_DIFFERENTIAL
void ads1256_set_mode(uint8_t mode)
{
    if (mode == SCANMODE_SINGLE_ENDED) {
        scan_mode = SCANMODE_SINGLE_ENDED;
    } else {
        scan_mode = SCANMODE_DIFFERENTIAL;
    }
}

void ads1256_write_command(uint8_t cmd)
{
    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);
    bcm2835_spi_transfer(cmd);
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
}

void ads1256_configure_adc(uint8_t _gain, uint8_t _drate)
{
    ads1256_wait_drdy_timeout();
    
    // status reg (id id id id order acal bufen drdy)
    //              0  0  1  1   0     1    0     0
    uint8_t status = 0x34;          // Status reg (0011 0100)
    // mux reg (psel3 psel2 psel1 psel0 nsel3 nsel2 nsel1 nsel0) 1xxx = AINCOM
    uint8_t mux = 0x08;             // MUX reg (0000 1000)
    uint8_t adcon = _gain & 0x07;   // ADCON reg (0000 0ggg)
    uint8_t drate = _drate;         // DRATE reg
    
    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);  // drive CS low
    
    bcm2835_spi_transfer(CMD_WREG | REG_STATUS); // 1st byte: start at status reg (address 0x00)
    bcm2835_spi_transfer(0x03); // 2nd byte: write 4 registers

    // transfer 4 bytes
    bcm2835_spi_transfer(status);
    bcm2835_spi_transfer(mux);
    bcm2835_spi_transfer(adcon);
    bcm2835_spi_transfer(drate);
    
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);  // drive CS high
    
    bcm2835_delay(1);
}

void ads1256_set_channel(uint8_t channel)
{
    if (channel > 7) {
        return ;
    }
    ads1256_write_register(REG_MUX, (channel << 4) | (1 << 3));
}

void ads1256_set_channel_differential(uint8_t channel)
{
    switch (channel) {
        case 0:
            ads1256_write_register(REG_MUX, (0 << 4) | 1);
            break;
        case 1:
            ads1256_write_register(REG_MUX, (2 << 4) | 3);
            break;
        case 2:
            ads1256_write_register(REG_MUX, (4 << 4) | 5);
            break;
        case 3:
            ads1256_write_register(REG_MUX, (6 << 4) | 7);
            break;
    }
}

void ads1256_reset()
{
    bcm2835_gpio_write(DEVICE_RST_PIN, HIGH);
    bcm2835_delay(200);
    bcm2835_gpio_write(DEVICE_RST_PIN, LOW);
    bcm2835_delay(200);
    bcm2835_gpio_write(DEVICE_RST_PIN, HIGH);
}

void ads1256_pin_reset()
{
    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);
    bcm2835_delayMicroseconds(10);
    bcm2835_spi_transfer(CMD_RESET);
    bcm2835_delay(2);
    bcm2835_spi_transfer(CMD_SDATAC);
    bcm2835_delayMicroseconds(100);
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
}

void ads1256_enter_standby_mode()
{
    ads1256_write_command(CMD_STANDBY);
}

void ads1256_write_register(uint8_t reg, uint8_t value)
{
    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);
    
    bcm2835_spi_transfer(CMD_WREG | reg);
    bcm2835_spi_transfer(0x00);
    bcm2835_spi_transfer(value);
    
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
}

uint8_t ads1256_read_register(uint8_t reg)
{
    uint8_t value = 0;

    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);
    
    bcm2835_spi_transfer(CMD_RREG | reg);
    bcm2835_spi_transfer(0x00);
    bcm2835_delayMicroseconds(500);
    value = bcm2835_spi_transfer(0xff);
    
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
    return value;
}

uint8_t ads1256_read_status_register()
{
    uint8_t value = 0;
    value = ads1256_read_register(REG_STATUS);
    return value;
}

void ads1256_get_n_samples(uint8_t channel, uint32_t* samples, uint32_t n_samples)
{
//    uint8_t buffer[3] = {0, 0, 0};
    uint32_t count = 0;

    while(bcm2835_gpio_lev(DEVICE_DRDY_PIN) == HIGH);
    ads1256_set_channel(channel);
    ads1256_write_command(CMD_SYNC);
    ads1256_write_command(CMD_WAKEUP);
    
    while (count < n_samples) {
        samples[count] = ads1256_get_single_continuous();

        count++;
    }

    printf("sample count: %d\n", count);
    ads1256_stop_continuous();
}

uint32_t ads1256_get_single_continuous()
{
    uint8_t buffer[3] = {0, 0, 0};
    uint32_t sample = 0x00000000;

    if (continuous_mode == false) {
        continuous_mode = true;
        bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);

        while (bcm2835_gpio_lev(DEVICE_DRDY_PIN) == HIGH);
        bcm2835_spi_transfer(CMD_RDATAC);
        bcm2835_delayMicroseconds(7);
    } else {
        while (bcm2835_gpio_lev(DEVICE_DRDY_PIN));
//        bcm2835_delayMicroseconds(5);
    }

    bcm2835_spi_transfern((char*)buffer, sizeof(buffer));
    sample = shift(buffer[0], buffer[1], buffer[2]);

    return sample;
}

void ads1256_stop_continuous()
{
    bcm2835_spi_transfer(CMD_SDATAC);
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
    continuous_mode = false;
}

uint32_t ads1256_get_single_sample(void)
{
    uint8_t buffer[3] = {0, 0, 0};
    uint32_t value = 0;

    ads1256_wait_drdy_timeout();
    bcm2835_delayMicroseconds(10);
    bcm2835_gpio_write(DEVICE_CS0_PIN, LOW);
    
    bcm2835_spi_transfer(CMD_RDATA);
    bcm2835_delayMicroseconds(10);
    bcm2835_spi_transfern((char*)buffer, sizeof(buffer));
    
    bcm2835_gpio_write(DEVICE_CS0_PIN, HIGH);
    
    value = shift(buffer[0], buffer[1], buffer[2]);
    
    return value;
}

uint32_t ads1256_get_sample_from_channel(uint8_t channel)
{
    uint32_t value = 0;
    
    while (bcm2835_gpio_lev(DEVICE_DRDY_PIN) == HIGH);
    
    if (scan_mode == SCANMODE_SINGLE_ENDED) {
        if (channel > 7) { // single channels 0 - 7
            return 0;
        }
        ads1256_set_channel(channel);
        ads1256_write_command(CMD_SYNC);
        ads1256_write_command(CMD_WAKEUP);
        value = ads1256_get_single_sample();
    } else {
        if (channel > 3) { // diff channels 0 - 3
            return 0;
        }
        ads1256_set_channel_differential(channel);
        ads1256_write_command(CMD_SYNC);
        ads1256_write_command(CMD_WAKEUP);
        value = ads1256_get_single_sample();
    }
    return value;
}

void ads1256_get_all_channels(uint32_t *values)
{
    for (uint8_t k = 0; k < 8; k++) {
        values[k] = ads1256_get_sample_from_channel(k);
    }
}

float convert_to_volts(uint32_t value)
{
    if ((value >> 23) == 1) {
        value -= 16777216;
    }
    return ((2 * VREF) / OFFSET) * value;
}

